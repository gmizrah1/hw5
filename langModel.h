/*600.120 Spring 2015 Homework 5
 * 4-1-2015
 * Gabriela Mizrahi-Arnaud
 * gmizrahi@jhu.edu gmizrah1
 * 617-515-9412
 * Ariel Goldschlag 
 * agoldsc2@jhu.edu agoldsc2
 * 516-368-4159
 */
#ifndef GUARD_langModel
#define GUARD_langModel

#include <unordered_map>
#include <string>
#include <list>
#include <vector>
#include <iostream>
#include <iterator>
#include <algorithm>
#include <cstdlib>
#include <ctime>

//struct for language model that contains three maps: 
//1)wordCount: maps from every word in input
//to number of times it appears in input
//2)nextWord: maps from every word in input 
//to a list of the words which follow it in the input
//3)numPairs: maps from every pair of words in input
//to the number of times it appears in the text
struct LangModel
{
  std::unordered_map<std::string, int> wordCount;
  std::unordered_map<std::string, std::list<std::string>> nextWord;
  std::unordered_map<std::string, int> numPairs;
};

//reads input text from std in and saves it as vector

std::vector<std::string> loadInput();

//takes in a vector of strings and returns a map from words to number of times they appear in the vector
std::unordered_map<std::string, int> countWords(std::vector<std::string>);

//takes in a vector of strings and returns a map from words to a list of words that follow it in the vector (without duplicates)
std::unordered_map<std::string, std::list<std::string>> checkNextWord(std::vector<std::string>);

//takes in a vector of strings and returns a map from word pairs to the number of times they appear
std::unordered_map<std::string, int> updateNumPair(std::vector<std::string>);

//returns a LangModel(see above) using vector of strings
LangModel createLangModel(std::vector<std::string>);

//returns a random number between 1 and the parameter with an equal distribution
int makeRand(int);

//returns the word that should probabilistically follow the string parameter, given the language model
std::string nextWord(std::string, LangModel);

//implemented for testing purposes: returns the word that will follow the string
//parameter given the language model if the random number is the parameter int
std::string genNextWord(LangModel,std::string, int);

//returns a vector of strings of randomly generated output based on language model 
std::vector<std::string> langOutput(LangModel);

//prints a vector of strings
void printOutput(std::vector<std::string>);
#endif 

/*600.120 Spring 2015 Homework 5
 * 4-1-2015
 * Gabriela Mizrahi-Arnaud
 * gmizrahi@jhu.edu gmizrah1
 * 617-515-9412
 * Ariel Goldschlag 
 * agoldsc2@jhu.edu agoldsc2
 * 516-368-4159
 */
#include "langModel.h"
#include <iostream>
using std::cout;
using std::cin;
using std::endl;
using std::string;
using std::unordered_map;
using std::vector;
using std::list;
using std::pair;

vector<string> loadInput()
{
  vector<string> input;
  input.push_back("<START>");   //add in <START> at begginning of vector for langModel
  string curword;
  while(cin>>curword)
  {
    input.push_back(curword);
  }
  input.push_back("<END>");   //add in <END> at end of vector for langModel  
  return input;
}

unordered_map<string,int> countWords(vector<string> input)
{
  unordered_map<string, int> wordCount;
  for(unsigned int i=0;i<input.size();i++)
  {
    wordCount[input.at(i)]++;
  }
  return wordCount;
}

unordered_map<string,list<string>> checkNextWord(vector<string> input)
{
 unordered_map<string, list<string>> nextWord;
 
 //for each element of vector
 for(unsigned int i=0; i<input.size()-1;i++)
  {
    if(nextWord.find(input.at(i))!= nextWord.end()) //if first word has already appeared
    {
        nextWord[input.at(i)].push_back(input.at(i+1));     
    }
    else    //if first appearance of first word, create the list
    {
      list<string> wordList;
      wordList.push_back(input.at(i+1));
      nextWord.insert(pair<string, list<string>>(input.at(i),wordList));
    }
  }

  //to remove duplicate following words,iterate through every list, sort and remove duplicates
 unordered_map<string,list<string>>::iterator nextWordI=nextWord.begin(); 
  while(nextWordI!=nextWord.end())
  {
    nextWordI->second.sort();
    nextWordI->second.unique();
    nextWordI++;
  }
  return nextWord;
}


unordered_map<string,int> updateNumPair(vector<string> input)
{
  unordered_map<string, int> numPair;
  for(unsigned int i=0;i<input.size()-1;i++)
  {
    string pair = input.at(i) + input.at(i+1);
    numPair[pair]++;
  }
  return numPair;
}


LangModel createLangModel(vector<string> input)
{
   LangModel model;
   model.wordCount=countWords(input);
   model.nextWord=checkNextWord(input);
   model.numPairs=updateNumPair(input);
   return model;
}//end createLangModel

int makeRand(int upperBound)
{
  int tooBig = (RAND_MAX / upperBound)*upperBound;
  int number = RAND_MAX;
  while(number > tooBig)
  {
  number = rand();
  }
  return (number % upperBound) +1;
}//end makeRand

string nextWord(string curWord,LangModel model)
{
  int numTimes=model.wordCount[curWord];
  int key = makeRand(numTimes);
  return genNextWord(model, curWord, key);
}//end nextWord
  
string genNextWord(LangModel model, string curWord, int key)
{
  string nextWord;
  list<string>::const_iterator nextWordI;
  nextWordI = model.nextWord[curWord].begin();
  
  //to get the probabilistically following word, iterate through list until
  //key==0 (key is randomly selected between 1 and number of times curWord appears)
  while( key >0 && nextWordI != model.nextWord[curWord].end())
  {
  nextWord = *nextWordI;
  key-=model.numPairs.at((curWord+nextWord));
  nextWordI++;
  }
  return nextWord;
}//end genNextWord

vector<string> langOutput(LangModel model)
{
  vector<string> output;
  string word="<START>";
  while(word!="<END>")
  {
    word=nextWord(word,model);
    output.push_back(word);
  }
  output.pop_back();//to delete "<END>" from the vector
  return output; 
}//end langOutput

void printOutput(vector<string> output)
{
  cout<<endl;
  for(unsigned int i=0; i<output.size(); i++)
  {
    cout<<output.at(i)<<" ";
  }
  cout<<endl;
}//end printOutput

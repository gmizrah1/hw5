#600.120 Spring 2015 Homework 5
# 4-1-2015
# Gabriela Mizrahi-Arnaud
# gmizrahi@jhu.edu gmizrah1
# 617-515-9412
# Ariel Goldschlag 
# agoldsc2@jhu.edu agoldsc2
#516-368-4159

CC=g++
#GCOV= -fprofile-arcs -ftest-coverage
CXXFLAGS= -std=c++11 -pedantic -Wall -Wextra -g
progs=testLangModel hw5
bin:hw5

test: testLangModel
	@echo "Testing..."
	./testLangModel
	@echo "Passed all tests."
testLangModel: testLangModel.cc langModel.o
langModel.o: langModel.cc langModel.h

hw5.o: hw5.cc langModel.h
hw5: hw5.o langModel.o 

testgcov: test
	 gcov *.cc
	 cat *.cc.gcov

clean:
	rm -f *.o $(progs) *.gcno *.gcda *.cc.gcov



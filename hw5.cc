/*600.120 Spring 2015 Homework 5
 * 4-1-2015
 * Gabriela Mizrahi-Arnaud
 * gmizrahi@jhu.edu gmizrah1
 * 617-515-9412
 * Ariel Goldschlag 
 * agoldsc2@jhu.edu agoldsc2
 * 516-368-4159
 */
#include "langModel.h"
using std::cout;
using std::endl;
using std::string;
using std::vector;

int main()
{
  srand(time(NULL));

  cout << "input text" << endl;
  vector<string> input =  loadInput();
  
  LangModel model=createLangModel(input);
  
  vector<string> output=langOutput(model);
  printOutput(output);
   return 0;
}

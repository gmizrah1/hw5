/*600.120 Spring 2015 Homework 5
 * 4-1-2015
 * Gabriela Mizrahi-Arnaud
 * gmizrahi@jhu.edu gmizrah1
 * 617-515-9412
 * Ariel Goldschlag 
 * agoldsc2@jhu.edu agoldsc2
 * 516-368-4159
 */
#include "langModel.h"
#include <cassert>
using std::cout;
using std::cin;
using std::endl;
using std::string;
using std::unordered_map;
using std::vector;
using std::list;

void test_genNextWord() {
    vector<string> inSeq = { "<START>", "he", "liked", "that", "he", "ate", "the", "sandwich", "he", "liked","<END>" };
    LangModel lm = createLangModel(inSeq);
    assert(genNextWord(lm, "he", 2) == "liked");
    assert(genNextWord(lm, "he", 1) == "ate");
    assert(genNextWord(lm, "liked", 2) == "that");
    assert(genNextWord(lm, "liked", 1) == "<END>");
    assert(genNextWord(lm, "<START>", 1) == "he");
}

void test_countWords()
{
  vector<string> inSeq = { "<START>", "he", "liked", "that", "he", "ate", "the", "sandwich", "he", "liked","<END>" };
    unordered_map<string,int> firstCounts = countWords(inSeq);
    assert(firstCounts["<START>"] == 1);
    assert(firstCounts["he"] == 3);
    assert(firstCounts["liked"] == 2);
    assert(firstCounts["that"] == 1);
    assert(firstCounts["ate"] == 1);
    assert(firstCounts["the"] == 1);
    assert(firstCounts["sandwich"] == 1);
    assert(firstCounts["<END>"] == 1);
}    

void test_checkNextWord()
{
  vector<string> inSeq = { "<START>", "he", "liked", "that", "he", "ate", "the", "sandwich", "he", "liked","<END>" };
    unordered_map<string, list<string>> follows = checkNextWord(inSeq);
    assert(follows["<START>"].size() == 1);
    assert(follows["he"].size() == 2);
    assert(follows["liked"].size() == 2);
    assert(follows["that"].size() == 1);
    assert(follows["ate"].size() == 1);
    assert(follows["the"].size() == 1);
    assert(follows["sandwich"].size() == 1);
    assert(follows["<END>"].size() == 0);
}

void test_updateNumPair()
{

  vector<string> inSeq = { "<START>", "he", "liked", "that", "he", "ate", "the", "sandwich", "he", "liked","<END>" };
  unordered_map<string, int> pairCounts = updateNumPair(inSeq);
  assert(pairCounts[("<START>he")] == 1);
  assert(pairCounts[("heliked")] == 2);
  assert(pairCounts[("likedthat")] == 1);
  assert(pairCounts[("thathe")] == 1);
  assert(pairCounts[("heate")] == 1);
  assert(pairCounts[("atethe")] == 1);
  assert(pairCounts[("thesandwich")] == 1);
  assert(pairCounts[("sandwichhe")] == 1);
  assert(pairCounts[("liked<END>")] == 1);

}
void test_makeRand()
{
  int randNum;
  for (int i=1;i<10000;i++)
  {
    randNum = makeRand(i);
    assert(randNum >=1 && randNum <= i);
  }
}
int main()
{
  srand(time(NULL));
  test_genNextWord();
  test_countWords();
  test_checkNextWord();
  test_updateNumPair();
  test_makeRand();
  return 0;
}
